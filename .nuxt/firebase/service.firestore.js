

export default async function (session) {
  await import(/* webpackChunkName: 'firebase-firestore' */'firebase/firestore/memory')

  const firestoreService = session.firestore()

  return firestoreService
}
