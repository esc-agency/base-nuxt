import Vue from 'vue'
import { wrapFunctional } from './index'

const components = {
  TheNavigator: () => import('../..\\components\\TheNavigator.vue' /* webpackChunkName: "components/the-navigator" */).then(c => wrapFunctional(c.default || c))
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}
