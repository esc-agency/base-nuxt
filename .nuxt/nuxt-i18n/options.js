export const vueI18n = {"fallbackLocale":"fr","silentTranslationWarn":true}
export const vueI18nLoader = false
export const locales = [{"name":"English","nameShort":"EN","code":"en","iso":"en-CA","file":"en-CA.js"},{"name":"Français","nameShort":"FR","code":"fr","iso":"fr-CA","file":"fr-CA.js","isCatchallLocale":true}]
export const defaultLocale = "fr"
export const defaultDirection = "ltr"
export const routesNameSeparator = "___"
export const defaultLocaleRouteNameSuffix = "default"
export const strategy = "prefix_except_default"
export const lazy = true
export const langDir = "C:\\Dev-esc\\base-nuxt\\lang"
export const rootRedirect = null
export const detectBrowserLanguage = false
export const differentDomains = false
export const seo = true
export const baseUrl = "https://www.jeanclaudedubeavocat.com"
export const vuex = {"moduleName":"i18n","syncLocale":false,"syncMessages":false,"syncRouteParams":true}
export const parsePages = false
export const pages = {"index":{"fr":"/","en":"/"},"experience":{"fr":"/experience","en":"/experience"},"services/droit-criminel-penal":{"fr":"/services/droit-criminel-penal","en":"/services/criminal-penal-law"},"services/droit-disciplinaire":{"fr":"/services/droit-disciplinaire-professionnel","en":"/services/disciplinary-professional-law"},"services/droit-formation":{"fr":"/services/formations-en-droit","en":"/services/training-in-law"},"equipe":{"fr":"/equipe","en":"/team"},"blogue/index":{"fr":"/blogue","en":"/blog"},"contact":{"fr":"/nous-joindre","en":"/contact-us"}}
export const skipSettingLocaleOnNavigate = false
export const beforeLanguageSwitch = () => null
export const onLanguageSwitched = () => null
export const IS_UNIVERSAL_MODE = true
export const MODULE_NAME = "nuxt-i18n"
export const LOCALE_CODE_KEY = "code"
export const LOCALE_ISO_KEY = "iso"
export const LOCALE_DIR_KEY = "dir"
export const LOCALE_DOMAIN_KEY = "domain"
export const LOCALE_FILE_KEY = "file"
export const STRATEGIES = {"PREFIX":"prefix","PREFIX_EXCEPT_DEFAULT":"prefix_except_default","PREFIX_AND_DEFAULT":"prefix_and_default","NO_PREFIX":"no_prefix"}
export const COMPONENT_OPTIONS_KEY = "nuxtI18n"
export const localeCodes = ["en","fr"]
export const trailingSlash = undefined

export const ASYNC_LOCALES = {
  'en-CA.js': () => import('../..\\lang\\en-CA.js' /* webpackChunkName: "lang-en-CA.js" */),
  'fr-CA.js': () => import('../..\\lang\\fr-CA.js' /* webpackChunkName: "lang-fr-CA.js" */)
}
