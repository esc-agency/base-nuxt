/* eslint-disable no-restricted-syntax */
import Vue from "vue";

export const state = () => ({
    scroll: {
        isScrolling: false,
        limit: 0,
        x: 0,
        y: 0
    },
    toggleSidebar: false,
    toggleFenetresNav: false,
    togglePortesNav: false,
    /* toggleMobileNav: false */
});

export const store = Vue.observable({
    isNavSmall: false
});

export const mutations = {
    setScroll: (state, payload) => {
        /* state.scroll = Object.assign({}, payload.state.scroll); */
        state.scroll = Object.assign({}, state.scroll, payload.slice);
    },
    shrinkNav () {
        // console.debug("mutation.shrinkNav");
        store.isNavSmall = true;
    },
    growNav () {
        // console.debug("mutation.growNav");
        store.isNavSmall = false;
    },
    TOGGLE_SIDEBAR (state) {
        state.toggleSidebar = !state.toggleSidebar;
    },
    TOGGLE_FENETRESNAV (state) {
        state.toggleFenetresNav = !state.toggleFenetresNav;
    },
    TOGGLE_PORTESNAV (state) {
        state.togglePortesNav = !state.togglePortesNav;
    },
    /* TOGGLE_MOBILENAV (state) {
        state.toggleMobileNav = !state.toggleMobileNav;
    } */
};

// actions
export const actions = {
    toggleSidebar ({ commit }) {
        commit("TOGGLE_SIDEBAR");
    },
    toggleFenetresNav ({ commit }) {
        commit("TOGGLE_FENETRESNAV");
    },
    togglePortesNav ({ commit }) {
        commit("TOGGLE_PORTESNAV");
    },
    /* toggleMobileNav ({ commit }) {
        commit("TOGGLE_MOBILENAV");
    } */
};

// Getters
export const getters = {
    toggleSidebar: state => state.toggleSidebar,
    toggleFenetresNav: state => state.toggleFenetresNav,
    togglePortesNav: state => state.togglePortesNav,
    /* toggleMobileNav: state => state.toggleMobileNav */
};
