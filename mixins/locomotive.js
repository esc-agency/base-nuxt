/* eslint-disable no-undef */
/* eslint-disable new-cap */
import gsap from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);

export default {
    mounted () {
        this.initScrolltrigger();
    },

    methods: {
        initScrolltrigger () {
            const locomotive = this.$refs.scroller.locomotive;
            locomotive.on("resize", ScrollTrigger.update);
            ScrollTrigger.scrollerProxy(locomotive.el, {
                scrollTop (value) {
                    return arguments.length ? locomotive.scrollTo(value, 0, 0) : locomotive.scroll.instance.scroll.y;
                },
                getBoundingClientRect () {
                    return { top: 0, left: 0, width: window.innerWidth, height: window.innerHeight };
                }
            });
        }
    },

    transition: {
        name: "page",
        css: true,
        beforeLeave () {
            document.documentElement.classList.remove("is-ready");
            document.documentElement.classList.add("is-loading");
        },
        beforeEnter () {
            document.documentElement.classList.remove("is-loading");
        },
        enter () {
            document.documentElement.classList.add("is-ready");
        },
    },
};
