export default {
    recaptcha: {
        siteKey: process.env.RECAPTCHA_SITE_KEY
    },
    gtm: {
        id: process.env.GOOGLE_TAG_MANAGER_ID
    }
};
