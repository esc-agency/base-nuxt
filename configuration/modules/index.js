export default [
    "@nuxtjs/gtm",
    "@nuxtjs/recaptcha",
    "nuxt-i18n",
    "@nuxt/content",
    ["nuxt-fontawesome", {
        imports: [
            {
                set: "@fortawesome/free-solid-svg-icons",
                icons: ["fas"]
            },
            {
                set: "@fortawesome/free-brands-svg-icons",
                icons: ["fab"]
            }
        ]
    }]
];
