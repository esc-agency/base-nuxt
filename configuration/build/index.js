export default {
    transpile: [
    // Vendors
        "gsap",
        "vee-validate/dist/rules",
    ],
    analyze: false,
    html:{
        minify: {
            collapseBooleanAttributes: true,
            decodeEntities: true,
            minifyCSS: true,
            minifyJS: true,
            processConditionalComments: true,
            removeEmptyAttributes: true,
            removeRedundantAttributes: true,
            trimCustomFragments: true,
            useShortDoctype: true,
            preserveLineBreaks: false,
            collapseWhitespace: true
        }
    },
    // Run ESLINT on save
    extend (config, ctx) {
        if (ctx.isClient) {
            config.module.rules.push({
                enforce: "pre",
                test: /\.(js|vue)$/,
                loader: "eslint-loader",
                exclude: /(node_modules)/
            });
        }
        config.node = {
            fs: "empty"
        };
    },
    hotMiddleware: {
        client: {
            overlay: false
        }
    },
    postcss: [
        require("autoprefixer")({
            overrideBrowserslist: ["> 5%"]
        })
    ]
};
