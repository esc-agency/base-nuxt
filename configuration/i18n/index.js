export default {
    baseUrl: "https://www.jeanclaudedubeavocat.com",
    defaultLocale: "fr",
    detectBrowserLanguage: false,
    seo: true,
    strategy: "prefix_except_default",
    lazy: true,
    langDir: "lang/",
    locales: [
        {
            name: "English",
            nameShort: "EN",
            code: "en",
            iso: "en-CA",
            file: "en-CA.js"
        },
        {
            name: "Français",
            nameShort: "FR",
            code: "fr",
            iso: "fr-CA",
            file: "fr-CA.js",
            isCatchallLocale: true
        }
    ],
    parsePages: false,
    pages: {
        "index": {
            fr: "/", // -> accessible at /about-us (no prefix since it's the default locale)
            en: "/", // -> accessible at /en/experience
        },
        "blogue/index": {
            fr: "/blogue",
            en: "/blog",
        },
        "contact": {
            fr: "/nous-joindre",
            en: "/contact-us",
        },
    },
    vueI18n: {
        fallbackLocale: "fr",
        silentTranslationWarn: true
    },
};
