export default [
    { src: "~/plugins/client", mode: "client" },
    { src: "~/plugins/global" },
    { src: "~/plugins/router.js" },
    { src: "~/plugins/validation" },
    { src: "~/plugins/i18n" }
];