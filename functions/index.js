/* eslint-disable no-unused-vars */
/* eslint-disable consistent-return */
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const cors = require("cors")({
    origin: [
        "http://localhost:3013",
        "http://localhost:5000",
        "https://fenetres-jalmo.web.app",
        "https://www.fenetresjalmo.com"
    ],
    methods: "GET"
});
admin.initializeApp();

exports.sendEmail = functions.region("northamerica-northeast1").firestore.document("contacts/{id}").onCreate(async (snap, context) => {
    const mailgun = require("mailgun-js")({apiKey: functions.config().mailgun.key, domain: functions.config().mailgun.domain, host: functions.config().mailgun.host});
    const Handlebars = require("handlebars");
    const fs = require("fs");
    const data = snap.data();
    const date = data.timestamp.toDate();
    const options = { timeZone: "America/New_York", weekday: "long", year: "numeric", month: "long", day: "numeric", hour: "numeric", minute: "numeric" };

    const fieldsTranslation = {
        "firstname": { "fr": "Prénom", "en": "First name" },
        "lastname": { "fr": "Nom", "en": "Last name" },
        "phone": { "fr": "Téléphone", "en": "Phone" },
        "email": { "fr": "Courriel", "en": "Email" },
        "services": { "fr": "Service", "en": "Service" },
        "message": { "fr": "Message", "en": "Message" },
    };

    const templateData = {
        logoPath: "https://firebasestorage.googleapis.com/",
        logoWidth: "274",
        logoHeight: "60",
        date: data.language === "fr" ? date.toLocaleString("fr-CA", options) : date.toLocaleString("en-CA", options),
        fields: [
            {"name": fieldsTranslation["firstname"][data.language], "value": data.firstname},
            {"name": fieldsTranslation["lastname"][data.language], "value": data.lastname},
            {"name": fieldsTranslation["email"][data.language], "value": data.email},
            {"name": fieldsTranslation["phone"][data.language], "value": data.phone},
            {"name": fieldsTranslation["services"][data.language], "value": data.services},
            {"name": fieldsTranslation["message"][data.message], "value": data.message}
        ]
    };

    const templateStr = fs.readFileSync("./template/email.hbs").toString("utf8");
    const template = Handlebars.compile(templateStr, { noEscape: true });

    const mail = {
        "from": "Fenêtres Jalmo <lead@everestcloud.io>",
        "to": data.email.includes("escaladeweb") ? functions.config().mailgun.bcc : functions.config().mailgun.to,
        "bcc": functions.config().mailgun.bcc,
        "subject": `${data.language === "fr" ? "Nouveau" : "New"} contact : ${data.firstname} ${data.lastname}`,
        "html": template(templateData),
        "o:tag": "Fenêtres Jalmo",
    };

    return mailgun.messages().send(mail, (error, body) => {
        if (error) {
            console.log(`FAIL: ${error}`);
        }
        else {
            console.log(`SUCCEED: ${body}`);
        }
    });
});

exports.sendRecaptcha = functions.region("northamerica-northeast1").https.onRequest(async (req, res) => {
    cors(req, res, async () => {
        const axios = require("axios");
        const USER_ERROR_CODES = ["missing-input-response", "invalid-input-response"];
        const secret = functions.config().recaptcha.secret;
        const token = req.query.token;

        try {
            const response = await axios.get(`https://recaptcha.google.com/recaptcha/api/siteverify?secret=${secret}&response=${token}`);

            const data = response.data;
            console.log("RESPONSE DATA: ", data);
            if (data.success) {
                return res.status(200).send({ score: data.score });
            }

            const errorCodes = data["error-codes"];
            if (errorCodes.length == 1 && USER_ERROR_CODES.includes(errorCodes[0])) {
                return res.status(400).send("Invalid Input");
            }
            return res.status(500).send("Internal Error");
        } catch (error) {
            console.log("error: ", error);
            res.status(500).send("Internal Error");
        }
    });
});
