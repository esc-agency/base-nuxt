import build from "./configuration/build";
import generate from "./configuration/generate";
import head from "./configuration/head";
import modules from "./configuration/modules";
import buildModules from "./configuration/buildModules";
import plugins from "./configuration/plugins";
import gtm from "./configuration/google-tag-manager";
import server from "./configuration/server";
import recaptcha from "./configuration/recaptcha";
import publicRuntimeConfig from "./configuration/publicRuntimeConfig";
import firebase from "./configuration/firebase";
import i18n from "./configuration/i18n";

export default {
    router: {},
    target: "static",
    components: true,
    ssr: false,
    modern: false,
    build,
    generate,
    head,
    modules,
    buildModules,
    plugins,
    gtm,
    server,
    recaptcha,
    publicRuntimeConfig,
    firebase,
    i18n,
    css: [
        "@/assets/css/template/styleguide.css",
        "@/assets/css/template/globals.css",
        "@/assets/css/main"
    ],
    loading: "~/components/TheLoader.vue",
    strict: false
};
