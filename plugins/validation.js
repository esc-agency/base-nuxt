import Vue from "vue";
import { ValidationProvider, ValidationObserver, extend } from "vee-validate";
// eslint-disable-next-line camelcase
import { required, alpha_spaces, regex, email } from "vee-validate/dist/rules";

Vue.component("ValidationProvider", ValidationProvider);
Vue.component("ValidationObserver", ValidationObserver);

extend("required", required);
extend("alpha_spaces", alpha_spaces);
extend("regex", regex);
extend("email", email);
