// https://medium.com/@ale_colombo/a-simple-multilanguage-site-with-nuxt-js-and-nuxt-i18n-43cce9f9f0fe
// fr-CA.js Traduction Française
import fr from "vee-validate/dist/locale/fr.json";

export default {
    validation: fr.messages,

    settings: {
        escalade: {
            right: "– Tous droits réservés.",
            design: "Conception et Marketing Internet",
            powered: "propulsés par"
        },
        recaptcha: {
            segment01: "Ce site est protégé par reCAPTCHA et les",
            segment02: "Règles de confidentialité",
            segment03: "et les",
            segment04: "Conditions d'utilisation",
            segment05: "de Google s'appliquent"
        }
    },
    meta: {
        baseUrl: "https://#.com/",
        baseUrlFb: "https://www.facebook.com/sharer/sharer.php?u=",
        baseUrlTwt: "https://twitter.com/home?status=",
        baseUrlLink: "https://www.linkedin.com/shareArticle?mini=true&amp;url=",
        baseTitle: " | Esc",
        accueil: {
            title: "Title",
            desc: "Dsc"
        }
    },

    form: {
        title: "Discutons de votre projet",
        input: {
            firstname: "Prénom",
            lastname: "Nom",
            email: "Courriel",
            phone: "Téléphone",
            city: "Ville",
            rappel: {
                default: "Période de rappel préférée",
                list: [
                    "Matin : 8 h - 12 h",
                    "Après-midi : 13 h  - 17 h",
                    "Soirée : 17 h - 21 h",
                ],
            },
            message: "Message",
            btn: "Soumettre",
        },
        validation: {
            successTitle: "Confirmation",
            success: "Votre message a été envoyé avec succès. Nous vous contacterons dans les plus brefs délais.",
            errored: "Dommage, une erreur s'est produite. SVP, réessayez ou appelez-nous au 450-694-2828",
        },
    },

    cta: {
        knowMore: "En savoir plus",
        rv: "Prendre rendez-vous",
        consult: "Obtenir une consultation",
        call: "Appelez-nous",
        send: "Soumettre",
        backHome: "Retour à l'accueil"
    },
};
