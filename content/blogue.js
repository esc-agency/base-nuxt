/* eslint-disable max-lines */
const articles = [
    {
        "blogue": "blogue",
        "path": "blogue",
        "slug": "guide-complet-pour-lachat-de-nouvelles-fenetres",

        "images": "fj-blogue-placeholder.png",
        "cat": "Conseils",
        "date": "12 Juillet 2021",
        "title": "Le guide complet pour l’achat de nouvelles fenêtres : 8 étapes importantes à suivre",
        "preview": "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides!",
        "intro": [
            "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides! Vous êtes à un clic de magasiner vos fenêtres sur la grande Toile mondiale, mais ne savez quelles étapes suivre pour circonscrire exactement votre choix? Du type d’ouvrant en passant par le bon cadre à sélectionner jusqu’à la quincaillerie de vos fenêtres, nous passons en revue tous les éléments!",
            "C’est ici qu’on vous vient en aide pour faire en sorte que vos futures fenêtres répondent en tous points à vos besoins, au style que vous recherchez et très certainement à votre budget. Par contre, gardez en mémoire qu’on ne peut magasiner seulement un prix lorsqu’il est question de fenêtres. L’enjeu de votre confort et de votre sécurité est trop important! Il faut donc vraiment prendre le temps de trouver un produit de qualité, performant et durable qui s'agence parfaitement à vos besoins esthétiques tout en respectant les sous que vous êtes prêts à verser.",
            "Voici donc sans plus tarder 8 étapes importantes à suivre pour l’achat de meilleures fenêtres! "
        ],
        "dsc": [
            {
                tag: "sousTitre",
                txt: "1re étape : pensez «municipalité"
            },
            {
                tag: "texte",
                txt: "Vous savez qu’un permis est requis pour certaines rénovations majeures à faire dans votre maison, mais est-ce le cas pour le remplacement de vos fenêtres? Pour le savoir, informez-vous auprès de votre municipalité. Celle-ci vous octroiera un permis si cela est prescrit. Aussi, vous serez surpris d'apprendre que, pour une question d’esthétique et d’uniformité, votre municipalité peut vous obliger à remplacer vos fenêtres par un style d’ouverture précis (fenêtres à battant ou à auvent, par exemple). Cette information influencera par ailleurs le choix que vous ferez ultérieurement. Il est donc important d’être au courant avant de faire l’achat de nouvelles fenêtres."
            },
            {
                tag: "sousTitre",
                txt: "2e étape :  quel type d’ouvrant choisir pour vos fenêtres?"
            },
            {
                tag: "texte",
                txt: "Fenêtres à battant, à auvent, coulissantes ou à guillotine? Il se peut que vous sachiez déjà ce qu’il vous faut comme type d’ouvrant ou que vous deviez suivre les règles municipales vous prescrivant le style d’ouverture; or, si ce n’est pas le cas, il faut tenir compte de ce chacune des ouvertures de fenêtres peut vous apporter en performance, en confort et en design. Prêt à les découvrir?"
            },

            {
                tag: "sousTitre",
                txt: "À battant"
            },
            {
                tag: "texte",
                txt: "Par exemple, si vous désirez une ouverture facile d’entretien, verticale, à manivelle, qui s’effectue vers l’extérieur et qui offre une performance supérieure ainsi qu’une excellente aération, votre choix s’arrêtera sur des fenêtres à battant, option particulièrement prisée au Québec."
            },
            {
                tag: "sousTitre",
                txt: "À auvent"
            },
            {
                tag: "texte",
                txt: "Or, si le look que vous désirez ressemble davantage à une ouverture horizontale, il se peut que les fenêtres à auvent offrant également une bonne ventilation ainsi qu’une excellente performance et permettant une ouverture vers l’extérieur soient votre choix de prédilection. Autre petit point à savoir : ces fenêtres peuvent demeurer ouvertes lorsqu’il pleut."
            },
            {
                tag: "sousTitre",
                txt: "Coulissantes"
            },
            {
                tag: "texte",
                txt: "Munies d’un système de loquet et de ressorts intégrés facilitant l’ouverture par un glissement horizontal, les fenêtres coulissantes, offertes en 2 ouvrants ou en un seul, prennent peu d’espace. Ce type de fenêtres qui s’agence bien aux styles contemporain et colonial se veut une option économique. "
            },
            {
                tag: "sousTitre",
                txt: "À guillotine"
            },
            {
                tag: "texte",
                txt: "Si vous êtes plus du style traditionnel, les fenêtres à guillotine, offrant une ouverture à bascule du volet vers l’intérieur, un système performant contre les infiltrations d’air et un système intégré de rejet d’eau, peuvent s’avérer un excellent choix."
            },
            {
                tag: "texte",
                txt: "Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure.Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure."
            },
            {
                tag: "sousTitre",
                txt: "3e étape : fenêtres en PVC ou hybrides?"
            },
            {
                tag: "texte",
                txt: "Une fois le type d’ouverture choisi vient le temps de sélectionner le cadre des fenêtres correspondant à vos besoins, à votre style et à votre budget. Voici certains critères à considérer pour chacun des types de cadres. "
            },
            {
                tag: "texte",
                txt: "Pour davantage de conseils et d’informations vous aidant à faire votre choix, nous vous invitons à contacter l’équipe-conseil de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "4e étape : efficacité énergétique"
            },
            {
                tag: "texte",
                txt: "Prévenez les pertes d’énergie et restez au chaud en optant pour des fenêtres certifiées ENERGY STAR. Celles-ci réduiront assurément vos factures d’électricité et vous assureront un doux confort. Par contre, il faut aussi considérer le facteur U (indiquant le degré de transmission de chaleur ou de froid par la fenêtre) lorsque vous achetez de nouvelles fenêtres. En fait, plus son facteur est faible, plus la fenêtre est performante et efficace. Ainsi, si certaines de vos fenêtres sont situées plus au soleil ou face au vent, le facteur U est vraiment à considérer et à évaluer. "
            },
            {
                tag: "sousTitre",
                txt: "5e étape : restez à l’abri des intempéries grâce aux fenêtres performantes"
            },
            {
                tag: "texte",
                txt: "Le choix d’une fenêtre s’arrête également sur son niveau de performance : est-elle non seulement solide et efficace, mais résiste-t-elle aussi au vent, à l’eau et à l’air? On se le rappelle : les fenêtres doivent être un gage de sécurité et de confort! Pour connaître leur niveau de performance, vous n’avez qu’à demander leurs résultats auprès de votre détaillant. Pour obtenir de bons conseils sur l'achat de fenêtres, n’hésitez pas à contacter l’équipe de Fenêtres Jalmo qui se fera un réel plaisir de vous aider."
            },
            {
                tag: "sousTitre",
                txt: "6e étape : prenez des mesures pour une estimation précise"
            },
            {
                tag: "texte",
                txt: "Hauteur, largeur et profondeur de vos fenêtres : si ce n’est déjà fait, sortez calepin ou cellulaire pour les y noter! N’oubliez pas dans vos calculs les moulures pour ce qui est de la hauteur et de la largeur! Ainsi, en ayant en mains toutes les mesures, vous pourrez obtenir une estimation plus juste en évitant de gros écarts de prix… et de mauvaises surprises. "
            },
            {
                tag: "sousTitre",
                txt: "7e étape : demande d’estimation de votre projet de fenêtres"
            },
            {
                tag: "texte",
                txt: "Étape importante qui influencera votre décision : l’estimation du prix de vos fenêtres (accessoires et quincaillerie) incluant la garantie et les frais d’installation! Jetez-y bien un coup d'œil pour que tous ces éléments-là s’y trouvent. Ne vous gênez pas aussi pour poser des questions sur les détails de votre estimation. L’achat de fenêtres est un gros investissement, il vaut mieux donc bien s’y préparer et savoir où iront les coûts. Pour une estimation de qualité et transparente, faites affaire avec l’équipe de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "8e étape : optez pour un manufacturier québécois"
            },
            {
                tag: "texte",
                txt: "En osant la fabrication locale de vos fenêtres, vous vous garantissez un produit de qualité, performant, durable, fiable et conçu pour toutes les saisons québécoises. Il ne faut donc pas lésiner sur la qualité supérieure d’une conception locale surtout quand il est question de votre confort et de votre sécurité. Une promesse que s’engage à tenir Fenêtres Jalmo, manufacturier québécois de fenêtres au service de votre bien-être."
            },
            {
                tag: "texte",
                txt: "Vous désirez remplacer vos portes et fenêtres pour des produits à haut rendement énergétique, de qualité, durables, performants et conçus au Québec ? Pour toutes questions ou une estimation, contactez l’équipe de Fenêtres Jalmo qui se fera un plaisir de vous servir !"
            }
        ]
    },

    {
        "path": "blogue",
        "slug": "guide-complet-pour-lachat-de-nouvelles-fenetres",

        "images": "fj-blogue-placeholder.png",
        "cat": "Conseils",
        "date": "12 Juillet 2021",
        "title": "Le guide complet pour l’achat de nouvelles fenêtres : 8 étapes importantes à suivre",
        "preview": "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides!",
        "intro": [
            "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides! Vous êtes à un clic de magasiner vos fenêtres sur la grande Toile mondiale, mais ne savez quelles étapes suivre pour circonscrire exactement votre choix? Du type d’ouvrant en passant par le bon cadre à sélectionner jusqu’à la quincaillerie de vos fenêtres, nous passons en revue tous les éléments!",
            "C’est ici qu’on vous vient en aide pour faire en sorte que vos futures fenêtres répondent en tous points à vos besoins, au style que vous recherchez et très certainement à votre budget. Par contre, gardez en mémoire qu’on ne peut magasiner seulement un prix lorsqu’il est question de fenêtres. L’enjeu de votre confort et de votre sécurité est trop important! Il faut donc vraiment prendre le temps de trouver un produit de qualité, performant et durable qui s'agence parfaitement à vos besoins esthétiques tout en respectant les sous que vous êtes prêts à verser.",
            "Voici donc sans plus tarder 8 étapes importantes à suivre pour l’achat de meilleures fenêtres! "
        ],
        "dsc": [
            {
                tag: "sousTitre",
                txt: "1re étape : pensez «municipalité"
            },
            {
                tag: "texte",
                txt: "Vous savez qu’un permis est requis pour certaines rénovations majeures à faire dans votre maison, mais est-ce le cas pour le remplacement de vos fenêtres? Pour le savoir, informez-vous auprès de votre municipalité. Celle-ci vous octroiera un permis si cela est prescrit. Aussi, vous serez surpris d'apprendre que, pour une question d’esthétique et d’uniformité, votre municipalité peut vous obliger à remplacer vos fenêtres par un style d’ouverture précis (fenêtres à battant ou à auvent, par exemple). Cette information influencera par ailleurs le choix que vous ferez ultérieurement. Il est donc important d’être au courant avant de faire l’achat de nouvelles fenêtres."
            },
            {
                tag: "sousTitre",
                txt: "2e étape :  quel type d’ouvrant choisir pour vos fenêtres?"
            },
            {
                tag: "texte",
                txt: "Fenêtres à battant, à auvent, coulissantes ou à guillotine? Il se peut que vous sachiez déjà ce qu’il vous faut comme type d’ouvrant ou que vous deviez suivre les règles municipales vous prescrivant le style d’ouverture; or, si ce n’est pas le cas, il faut tenir compte de ce chacune des ouvertures de fenêtres peut vous apporter en performance, en confort et en design. Prêt à les découvrir?"
            },

            {
                tag: "sousTitre",
                txt: "À battant"
            },
            {
                tag: "texte",
                txt: "Par exemple, si vous désirez une ouverture facile d’entretien, verticale, à manivelle, qui s’effectue vers l’extérieur et qui offre une performance supérieure ainsi qu’une excellente aération, votre choix s’arrêtera sur des fenêtres à battant, option particulièrement prisée au Québec."
            },
            {
                tag: "sousTitre",
                txt: "À auvent"
            },
            {
                tag: "texte",
                txt: "Or, si le look que vous désirez ressemble davantage à une ouverture horizontale, il se peut que les fenêtres à auvent offrant également une bonne ventilation ainsi qu’une excellente performance et permettant une ouverture vers l’extérieur soient votre choix de prédilection. Autre petit point à savoir : ces fenêtres peuvent demeurer ouvertes lorsqu’il pleut."
            },
            {
                tag: "sousTitre",
                txt: "Coulissantes"
            },
            {
                tag: "texte",
                txt: "Munies d’un système de loquet et de ressorts intégrés facilitant l’ouverture par un glissement horizontal, les fenêtres coulissantes, offertes en 2 ouvrants ou en un seul, prennent peu d’espace. Ce type de fenêtres qui s’agence bien aux styles contemporain et colonial se veut une option économique. "
            },
            {
                tag: "sousTitre",
                txt: "À guillotine"
            },
            {
                tag: "texte",
                txt: "Si vous êtes plus du style traditionnel, les fenêtres à guillotine, offrant une ouverture à bascule du volet vers l’intérieur, un système performant contre les infiltrations d’air et un système intégré de rejet d’eau, peuvent s’avérer un excellent choix."
            },
            {
                tag: "texte",
                txt: "Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure.Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure."
            },
            {
                tag: "sousTitre",
                txt: "3e étape : fenêtres en PVC ou hybrides?"
            },
            {
                tag: "texte",
                txt: "Une fois le type d’ouverture choisi vient le temps de sélectionner le cadre des fenêtres correspondant à vos besoins, à votre style et à votre budget. Voici certains critères à considérer pour chacun des types de cadres. "
            },
            {
                tag: "texte",
                txt: "Pour davantage de conseils et d’informations vous aidant à faire votre choix, nous vous invitons à contacter l’équipe-conseil de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "4e étape : efficacité énergétique"
            },
            {
                tag: "texte",
                txt: "Prévenez les pertes d’énergie et restez au chaud en optant pour des fenêtres certifiées ENERGY STAR. Celles-ci réduiront assurément vos factures d’électricité et vous assureront un doux confort. Par contre, il faut aussi considérer le facteur U (indiquant le degré de transmission de chaleur ou de froid par la fenêtre) lorsque vous achetez de nouvelles fenêtres. En fait, plus son facteur est faible, plus la fenêtre est performante et efficace. Ainsi, si certaines de vos fenêtres sont situées plus au soleil ou face au vent, le facteur U est vraiment à considérer et à évaluer. "
            },
            {
                tag: "sousTitre",
                txt: "5e étape : restez à l’abri des intempéries grâce aux fenêtres performantes"
            },
            {
                tag: "texte",
                txt: "Le choix d’une fenêtre s’arrête également sur son niveau de performance : est-elle non seulement solide et efficace, mais résiste-t-elle aussi au vent, à l’eau et à l’air? On se le rappelle : les fenêtres doivent être un gage de sécurité et de confort! Pour connaître leur niveau de performance, vous n’avez qu’à demander leurs résultats auprès de votre détaillant. Pour obtenir de bons conseils sur l'achat de fenêtres, n’hésitez pas à contacter l’équipe de Fenêtres Jalmo qui se fera un réel plaisir de vous aider."
            },
            {
                tag: "sousTitre",
                txt: "6e étape : prenez des mesures pour une estimation précise"
            },
            {
                tag: "texte",
                txt: "Hauteur, largeur et profondeur de vos fenêtres : si ce n’est déjà fait, sortez calepin ou cellulaire pour les y noter! N’oubliez pas dans vos calculs les moulures pour ce qui est de la hauteur et de la largeur! Ainsi, en ayant en mains toutes les mesures, vous pourrez obtenir une estimation plus juste en évitant de gros écarts de prix… et de mauvaises surprises. "
            },
            {
                tag: "sousTitre",
                txt: "7e étape : demande d’estimation de votre projet de fenêtres"
            },
            {
                tag: "texte",
                txt: "Étape importante qui influencera votre décision : l’estimation du prix de vos fenêtres (accessoires et quincaillerie) incluant la garantie et les frais d’installation! Jetez-y bien un coup d'œil pour que tous ces éléments-là s’y trouvent. Ne vous gênez pas aussi pour poser des questions sur les détails de votre estimation. L’achat de fenêtres est un gros investissement, il vaut mieux donc bien s’y préparer et savoir où iront les coûts. Pour une estimation de qualité et transparente, faites affaire avec l’équipe de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "8e étape : optez pour un manufacturier québécois"
            },
            {
                tag: "texte",
                txt: "En osant la fabrication locale de vos fenêtres, vous vous garantissez un produit de qualité, performant, durable, fiable et conçu pour toutes les saisons québécoises. Il ne faut donc pas lésiner sur la qualité supérieure d’une conception locale surtout quand il est question de votre confort et de votre sécurité. Une promesse que s’engage à tenir Fenêtres Jalmo, manufacturier québécois de fenêtres au service de votre bien-être."
            },
            {
                tag: "texte",
                txt: "Vous désirez remplacer vos portes et fenêtres pour des produits à haut rendement énergétique, de qualité, durables, performants et conçus au Québec ? Pour toutes questions ou une estimation, contactez l’équipe de Fenêtres Jalmo qui se fera un plaisir de vous servir !"
            }
        ]
    },

    {
        "path": "blogue",
        "slug": "guide-complet-pour-lachat-de-nouvelles-fenetres",

        "images": "fj-blogue-placeholder.png",
        "cat": "Conseils",
        "date": "12 Juillet 2021",
        "title": "Le guide complet pour l’achat de nouvelles fenêtres : 8 étapes importantes à suivre",
        "preview": "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides!",
        "intro": [
            "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides! Vous êtes à un clic de magasiner vos fenêtres sur la grande Toile mondiale, mais ne savez quelles étapes suivre pour circonscrire exactement votre choix? Du type d’ouvrant en passant par le bon cadre à sélectionner jusqu’à la quincaillerie de vos fenêtres, nous passons en revue tous les éléments!",
            "C’est ici qu’on vous vient en aide pour faire en sorte que vos futures fenêtres répondent en tous points à vos besoins, au style que vous recherchez et très certainement à votre budget. Par contre, gardez en mémoire qu’on ne peut magasiner seulement un prix lorsqu’il est question de fenêtres. L’enjeu de votre confort et de votre sécurité est trop important! Il faut donc vraiment prendre le temps de trouver un produit de qualité, performant et durable qui s'agence parfaitement à vos besoins esthétiques tout en respectant les sous que vous êtes prêts à verser.",
            "Voici donc sans plus tarder 8 étapes importantes à suivre pour l’achat de meilleures fenêtres! "
        ],
        "dsc": [
            {
                tag: "sousTitre",
                txt: "1re étape : pensez «municipalité"
            },
            {
                tag: "texte",
                txt: "Vous savez qu’un permis est requis pour certaines rénovations majeures à faire dans votre maison, mais est-ce le cas pour le remplacement de vos fenêtres? Pour le savoir, informez-vous auprès de votre municipalité. Celle-ci vous octroiera un permis si cela est prescrit. Aussi, vous serez surpris d'apprendre que, pour une question d’esthétique et d’uniformité, votre municipalité peut vous obliger à remplacer vos fenêtres par un style d’ouverture précis (fenêtres à battant ou à auvent, par exemple). Cette information influencera par ailleurs le choix que vous ferez ultérieurement. Il est donc important d’être au courant avant de faire l’achat de nouvelles fenêtres."
            },
            {
                tag: "sousTitre",
                txt: "2e étape :  quel type d’ouvrant choisir pour vos fenêtres?"
            },
            {
                tag: "texte",
                txt: "Fenêtres à battant, à auvent, coulissantes ou à guillotine? Il se peut que vous sachiez déjà ce qu’il vous faut comme type d’ouvrant ou que vous deviez suivre les règles municipales vous prescrivant le style d’ouverture; or, si ce n’est pas le cas, il faut tenir compte de ce chacune des ouvertures de fenêtres peut vous apporter en performance, en confort et en design. Prêt à les découvrir?"
            },

            {
                tag: "sousTitre",
                txt: "À battant"
            },
            {
                tag: "texte",
                txt: "Par exemple, si vous désirez une ouverture facile d’entretien, verticale, à manivelle, qui s’effectue vers l’extérieur et qui offre une performance supérieure ainsi qu’une excellente aération, votre choix s’arrêtera sur des fenêtres à battant, option particulièrement prisée au Québec."
            },
            {
                tag: "sousTitre",
                txt: "À auvent"
            },
            {
                tag: "texte",
                txt: "Or, si le look que vous désirez ressemble davantage à une ouverture horizontale, il se peut que les fenêtres à auvent offrant également une bonne ventilation ainsi qu’une excellente performance et permettant une ouverture vers l’extérieur soient votre choix de prédilection. Autre petit point à savoir : ces fenêtres peuvent demeurer ouvertes lorsqu’il pleut."
            },
            {
                tag: "sousTitre",
                txt: "Coulissantes"
            },
            {
                tag: "texte",
                txt: "Munies d’un système de loquet et de ressorts intégrés facilitant l’ouverture par un glissement horizontal, les fenêtres coulissantes, offertes en 2 ouvrants ou en un seul, prennent peu d’espace. Ce type de fenêtres qui s’agence bien aux styles contemporain et colonial se veut une option économique. "
            },
            {
                tag: "sousTitre",
                txt: "À guillotine"
            },
            {
                tag: "texte",
                txt: "Si vous êtes plus du style traditionnel, les fenêtres à guillotine, offrant une ouverture à bascule du volet vers l’intérieur, un système performant contre les infiltrations d’air et un système intégré de rejet d’eau, peuvent s’avérer un excellent choix."
            },
            {
                tag: "texte",
                txt: "Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure.Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure."
            },
            {
                tag: "sousTitre",
                txt: "3e étape : fenêtres en PVC ou hybrides?"
            },
            {
                tag: "texte",
                txt: "Une fois le type d’ouverture choisi vient le temps de sélectionner le cadre des fenêtres correspondant à vos besoins, à votre style et à votre budget. Voici certains critères à considérer pour chacun des types de cadres. "
            },
            {
                tag: "texte",
                txt: "Pour davantage de conseils et d’informations vous aidant à faire votre choix, nous vous invitons à contacter l’équipe-conseil de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "4e étape : efficacité énergétique"
            },
            {
                tag: "texte",
                txt: "Prévenez les pertes d’énergie et restez au chaud en optant pour des fenêtres certifiées ENERGY STAR. Celles-ci réduiront assurément vos factures d’électricité et vous assureront un doux confort. Par contre, il faut aussi considérer le facteur U (indiquant le degré de transmission de chaleur ou de froid par la fenêtre) lorsque vous achetez de nouvelles fenêtres. En fait, plus son facteur est faible, plus la fenêtre est performante et efficace. Ainsi, si certaines de vos fenêtres sont situées plus au soleil ou face au vent, le facteur U est vraiment à considérer et à évaluer. "
            },
            {
                tag: "sousTitre",
                txt: "5e étape : restez à l’abri des intempéries grâce aux fenêtres performantes"
            },
            {
                tag: "texte",
                txt: "Le choix d’une fenêtre s’arrête également sur son niveau de performance : est-elle non seulement solide et efficace, mais résiste-t-elle aussi au vent, à l’eau et à l’air? On se le rappelle : les fenêtres doivent être un gage de sécurité et de confort! Pour connaître leur niveau de performance, vous n’avez qu’à demander leurs résultats auprès de votre détaillant. Pour obtenir de bons conseils sur l'achat de fenêtres, n’hésitez pas à contacter l’équipe de Fenêtres Jalmo qui se fera un réel plaisir de vous aider."
            },
            {
                tag: "sousTitre",
                txt: "6e étape : prenez des mesures pour une estimation précise"
            },
            {
                tag: "texte",
                txt: "Hauteur, largeur et profondeur de vos fenêtres : si ce n’est déjà fait, sortez calepin ou cellulaire pour les y noter! N’oubliez pas dans vos calculs les moulures pour ce qui est de la hauteur et de la largeur! Ainsi, en ayant en mains toutes les mesures, vous pourrez obtenir une estimation plus juste en évitant de gros écarts de prix… et de mauvaises surprises. "
            },
            {
                tag: "sousTitre",
                txt: "7e étape : demande d’estimation de votre projet de fenêtres"
            },
            {
                tag: "texte",
                txt: "Étape importante qui influencera votre décision : l’estimation du prix de vos fenêtres (accessoires et quincaillerie) incluant la garantie et les frais d’installation! Jetez-y bien un coup d'œil pour que tous ces éléments-là s’y trouvent. Ne vous gênez pas aussi pour poser des questions sur les détails de votre estimation. L’achat de fenêtres est un gros investissement, il vaut mieux donc bien s’y préparer et savoir où iront les coûts. Pour une estimation de qualité et transparente, faites affaire avec l’équipe de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "8e étape : optez pour un manufacturier québécois"
            },
            {
                tag: "texte",
                txt: "En osant la fabrication locale de vos fenêtres, vous vous garantissez un produit de qualité, performant, durable, fiable et conçu pour toutes les saisons québécoises. Il ne faut donc pas lésiner sur la qualité supérieure d’une conception locale surtout quand il est question de votre confort et de votre sécurité. Une promesse que s’engage à tenir Fenêtres Jalmo, manufacturier québécois de fenêtres au service de votre bien-être."
            },
            {
                tag: "texte",
                txt: "Vous désirez remplacer vos portes et fenêtres pour des produits à haut rendement énergétique, de qualité, durables, performants et conçus au Québec ? Pour toutes questions ou une estimation, contactez l’équipe de Fenêtres Jalmo qui se fera un plaisir de vous servir !"
            }
        ]
    },

    {
        "path": "blogue",
        "slug": "guide-complet-pour-lachat-de-nouvelles-fenetres",

        "images": "fj-blogue-placeholder.png",
        "cat": "Conseils",
        "date": "12 Juillet 2021",
        "title": "Le guide complet pour l’achat de nouvelles fenêtres : 8 étapes importantes à suivre",
        "preview": "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides!",
        "intro": [
            "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides! Vous êtes à un clic de magasiner vos fenêtres sur la grande Toile mondiale, mais ne savez quelles étapes suivre pour circonscrire exactement votre choix? Du type d’ouvrant en passant par le bon cadre à sélectionner jusqu’à la quincaillerie de vos fenêtres, nous passons en revue tous les éléments!",
            "C’est ici qu’on vous vient en aide pour faire en sorte que vos futures fenêtres répondent en tous points à vos besoins, au style que vous recherchez et très certainement à votre budget. Par contre, gardez en mémoire qu’on ne peut magasiner seulement un prix lorsqu’il est question de fenêtres. L’enjeu de votre confort et de votre sécurité est trop important! Il faut donc vraiment prendre le temps de trouver un produit de qualité, performant et durable qui s'agence parfaitement à vos besoins esthétiques tout en respectant les sous que vous êtes prêts à verser.",
            "Voici donc sans plus tarder 8 étapes importantes à suivre pour l’achat de meilleures fenêtres! "
        ],
        "dsc": [
            {
                tag: "sousTitre",
                txt: "1re étape : pensez «municipalité"
            },
            {
                tag: "texte",
                txt: "Vous savez qu’un permis est requis pour certaines rénovations majeures à faire dans votre maison, mais est-ce le cas pour le remplacement de vos fenêtres? Pour le savoir, informez-vous auprès de votre municipalité. Celle-ci vous octroiera un permis si cela est prescrit. Aussi, vous serez surpris d'apprendre que, pour une question d’esthétique et d’uniformité, votre municipalité peut vous obliger à remplacer vos fenêtres par un style d’ouverture précis (fenêtres à battant ou à auvent, par exemple). Cette information influencera par ailleurs le choix que vous ferez ultérieurement. Il est donc important d’être au courant avant de faire l’achat de nouvelles fenêtres."
            },
            {
                tag: "sousTitre",
                txt: "2e étape :  quel type d’ouvrant choisir pour vos fenêtres?"
            },
            {
                tag: "texte",
                txt: "Fenêtres à battant, à auvent, coulissantes ou à guillotine? Il se peut que vous sachiez déjà ce qu’il vous faut comme type d’ouvrant ou que vous deviez suivre les règles municipales vous prescrivant le style d’ouverture; or, si ce n’est pas le cas, il faut tenir compte de ce chacune des ouvertures de fenêtres peut vous apporter en performance, en confort et en design. Prêt à les découvrir?"
            },

            {
                tag: "sousTitre",
                txt: "À battant"
            },
            {
                tag: "texte",
                txt: "Par exemple, si vous désirez une ouverture facile d’entretien, verticale, à manivelle, qui s’effectue vers l’extérieur et qui offre une performance supérieure ainsi qu’une excellente aération, votre choix s’arrêtera sur des fenêtres à battant, option particulièrement prisée au Québec."
            },
            {
                tag: "sousTitre",
                txt: "À auvent"
            },
            {
                tag: "texte",
                txt: "Or, si le look que vous désirez ressemble davantage à une ouverture horizontale, il se peut que les fenêtres à auvent offrant également une bonne ventilation ainsi qu’une excellente performance et permettant une ouverture vers l’extérieur soient votre choix de prédilection. Autre petit point à savoir : ces fenêtres peuvent demeurer ouvertes lorsqu’il pleut."
            },
            {
                tag: "sousTitre",
                txt: "Coulissantes"
            },
            {
                tag: "texte",
                txt: "Munies d’un système de loquet et de ressorts intégrés facilitant l’ouverture par un glissement horizontal, les fenêtres coulissantes, offertes en 2 ouvrants ou en un seul, prennent peu d’espace. Ce type de fenêtres qui s’agence bien aux styles contemporain et colonial se veut une option économique. "
            },
            {
                tag: "sousTitre",
                txt: "À guillotine"
            },
            {
                tag: "texte",
                txt: "Si vous êtes plus du style traditionnel, les fenêtres à guillotine, offrant une ouverture à bascule du volet vers l’intérieur, un système performant contre les infiltrations d’air et un système intégré de rejet d’eau, peuvent s’avérer un excellent choix."
            },
            {
                tag: "texte",
                txt: "Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure.Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure."
            },
            {
                tag: "sousTitre",
                txt: "3e étape : fenêtres en PVC ou hybrides?"
            },
            {
                tag: "texte",
                txt: "Une fois le type d’ouverture choisi vient le temps de sélectionner le cadre des fenêtres correspondant à vos besoins, à votre style et à votre budget. Voici certains critères à considérer pour chacun des types de cadres. "
            },
            {
                tag: "texte",
                txt: "Pour davantage de conseils et d’informations vous aidant à faire votre choix, nous vous invitons à contacter l’équipe-conseil de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "4e étape : efficacité énergétique"
            },
            {
                tag: "texte",
                txt: "Prévenez les pertes d’énergie et restez au chaud en optant pour des fenêtres certifiées ENERGY STAR. Celles-ci réduiront assurément vos factures d’électricité et vous assureront un doux confort. Par contre, il faut aussi considérer le facteur U (indiquant le degré de transmission de chaleur ou de froid par la fenêtre) lorsque vous achetez de nouvelles fenêtres. En fait, plus son facteur est faible, plus la fenêtre est performante et efficace. Ainsi, si certaines de vos fenêtres sont situées plus au soleil ou face au vent, le facteur U est vraiment à considérer et à évaluer. "
            },
            {
                tag: "sousTitre",
                txt: "5e étape : restez à l’abri des intempéries grâce aux fenêtres performantes"
            },
            {
                tag: "texte",
                txt: "Le choix d’une fenêtre s’arrête également sur son niveau de performance : est-elle non seulement solide et efficace, mais résiste-t-elle aussi au vent, à l’eau et à l’air? On se le rappelle : les fenêtres doivent être un gage de sécurité et de confort! Pour connaître leur niveau de performance, vous n’avez qu’à demander leurs résultats auprès de votre détaillant. Pour obtenir de bons conseils sur l'achat de fenêtres, n’hésitez pas à contacter l’équipe de Fenêtres Jalmo qui se fera un réel plaisir de vous aider."
            },
            {
                tag: "sousTitre",
                txt: "6e étape : prenez des mesures pour une estimation précise"
            },
            {
                tag: "texte",
                txt: "Hauteur, largeur et profondeur de vos fenêtres : si ce n’est déjà fait, sortez calepin ou cellulaire pour les y noter! N’oubliez pas dans vos calculs les moulures pour ce qui est de la hauteur et de la largeur! Ainsi, en ayant en mains toutes les mesures, vous pourrez obtenir une estimation plus juste en évitant de gros écarts de prix… et de mauvaises surprises. "
            },
            {
                tag: "sousTitre",
                txt: "7e étape : demande d’estimation de votre projet de fenêtres"
            },
            {
                tag: "texte",
                txt: "Étape importante qui influencera votre décision : l’estimation du prix de vos fenêtres (accessoires et quincaillerie) incluant la garantie et les frais d’installation! Jetez-y bien un coup d'œil pour que tous ces éléments-là s’y trouvent. Ne vous gênez pas aussi pour poser des questions sur les détails de votre estimation. L’achat de fenêtres est un gros investissement, il vaut mieux donc bien s’y préparer et savoir où iront les coûts. Pour une estimation de qualité et transparente, faites affaire avec l’équipe de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "8e étape : optez pour un manufacturier québécois"
            },
            {
                tag: "texte",
                txt: "En osant la fabrication locale de vos fenêtres, vous vous garantissez un produit de qualité, performant, durable, fiable et conçu pour toutes les saisons québécoises. Il ne faut donc pas lésiner sur la qualité supérieure d’une conception locale surtout quand il est question de votre confort et de votre sécurité. Une promesse que s’engage à tenir Fenêtres Jalmo, manufacturier québécois de fenêtres au service de votre bien-être."
            },
            {
                tag: "texte",
                txt: "Vous désirez remplacer vos portes et fenêtres pour des produits à haut rendement énergétique, de qualité, durables, performants et conçus au Québec ? Pour toutes questions ou une estimation, contactez l’équipe de Fenêtres Jalmo qui se fera un plaisir de vous servir !"
            }
        ]
    },

    {
        "path": "blogue",
        "slug": "guide-complet-pour-lachat-de-nouvelles-fenetres",

        "images": "fj-blogue-placeholder.png",
        "cat": "Conseils",
        "date": "12 Juillet 2021",
        "title": "Le guide complet pour l’achat de nouvelles fenêtres : 8 étapes importantes à suivre",
        "preview": "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides!",
        "intro": [
            "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides! Vous êtes à un clic de magasiner vos fenêtres sur la grande Toile mondiale, mais ne savez quelles étapes suivre pour circonscrire exactement votre choix? Du type d’ouvrant en passant par le bon cadre à sélectionner jusqu’à la quincaillerie de vos fenêtres, nous passons en revue tous les éléments!",
            "C’est ici qu’on vous vient en aide pour faire en sorte que vos futures fenêtres répondent en tous points à vos besoins, au style que vous recherchez et très certainement à votre budget. Par contre, gardez en mémoire qu’on ne peut magasiner seulement un prix lorsqu’il est question de fenêtres. L’enjeu de votre confort et de votre sécurité est trop important! Il faut donc vraiment prendre le temps de trouver un produit de qualité, performant et durable qui s'agence parfaitement à vos besoins esthétiques tout en respectant les sous que vous êtes prêts à verser.",
            "Voici donc sans plus tarder 8 étapes importantes à suivre pour l’achat de meilleures fenêtres! "
        ],
        "dsc": [
            {
                tag: "sousTitre",
                txt: "1re étape : pensez «municipalité"
            },
            {
                tag: "texte",
                txt: "Vous savez qu’un permis est requis pour certaines rénovations majeures à faire dans votre maison, mais est-ce le cas pour le remplacement de vos fenêtres? Pour le savoir, informez-vous auprès de votre municipalité. Celle-ci vous octroiera un permis si cela est prescrit. Aussi, vous serez surpris d'apprendre que, pour une question d’esthétique et d’uniformité, votre municipalité peut vous obliger à remplacer vos fenêtres par un style d’ouverture précis (fenêtres à battant ou à auvent, par exemple). Cette information influencera par ailleurs le choix que vous ferez ultérieurement. Il est donc important d’être au courant avant de faire l’achat de nouvelles fenêtres."
            },
            {
                tag: "sousTitre",
                txt: "2e étape :  quel type d’ouvrant choisir pour vos fenêtres?"
            },
            {
                tag: "texte",
                txt: "Fenêtres à battant, à auvent, coulissantes ou à guillotine? Il se peut que vous sachiez déjà ce qu’il vous faut comme type d’ouvrant ou que vous deviez suivre les règles municipales vous prescrivant le style d’ouverture; or, si ce n’est pas le cas, il faut tenir compte de ce chacune des ouvertures de fenêtres peut vous apporter en performance, en confort et en design. Prêt à les découvrir?"
            },

            {
                tag: "sousTitre",
                txt: "À battant"
            },
            {
                tag: "texte",
                txt: "Par exemple, si vous désirez une ouverture facile d’entretien, verticale, à manivelle, qui s’effectue vers l’extérieur et qui offre une performance supérieure ainsi qu’une excellente aération, votre choix s’arrêtera sur des fenêtres à battant, option particulièrement prisée au Québec."
            },
            {
                tag: "sousTitre",
                txt: "À auvent"
            },
            {
                tag: "texte",
                txt: "Or, si le look que vous désirez ressemble davantage à une ouverture horizontale, il se peut que les fenêtres à auvent offrant également une bonne ventilation ainsi qu’une excellente performance et permettant une ouverture vers l’extérieur soient votre choix de prédilection. Autre petit point à savoir : ces fenêtres peuvent demeurer ouvertes lorsqu’il pleut."
            },
            {
                tag: "sousTitre",
                txt: "Coulissantes"
            },
            {
                tag: "texte",
                txt: "Munies d’un système de loquet et de ressorts intégrés facilitant l’ouverture par un glissement horizontal, les fenêtres coulissantes, offertes en 2 ouvrants ou en un seul, prennent peu d’espace. Ce type de fenêtres qui s’agence bien aux styles contemporain et colonial se veut une option économique. "
            },
            {
                tag: "sousTitre",
                txt: "À guillotine"
            },
            {
                tag: "texte",
                txt: "Si vous êtes plus du style traditionnel, les fenêtres à guillotine, offrant une ouverture à bascule du volet vers l’intérieur, un système performant contre les infiltrations d’air et un système intégré de rejet d’eau, peuvent s’avérer un excellent choix."
            },
            {
                tag: "texte",
                txt: "Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure.Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure."
            },
            {
                tag: "sousTitre",
                txt: "3e étape : fenêtres en PVC ou hybrides?"
            },
            {
                tag: "texte",
                txt: "Une fois le type d’ouverture choisi vient le temps de sélectionner le cadre des fenêtres correspondant à vos besoins, à votre style et à votre budget. Voici certains critères à considérer pour chacun des types de cadres. "
            },
            {
                tag: "texte",
                txt: "Pour davantage de conseils et d’informations vous aidant à faire votre choix, nous vous invitons à contacter l’équipe-conseil de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "4e étape : efficacité énergétique"
            },
            {
                tag: "texte",
                txt: "Prévenez les pertes d’énergie et restez au chaud en optant pour des fenêtres certifiées ENERGY STAR. Celles-ci réduiront assurément vos factures d’électricité et vous assureront un doux confort. Par contre, il faut aussi considérer le facteur U (indiquant le degré de transmission de chaleur ou de froid par la fenêtre) lorsque vous achetez de nouvelles fenêtres. En fait, plus son facteur est faible, plus la fenêtre est performante et efficace. Ainsi, si certaines de vos fenêtres sont situées plus au soleil ou face au vent, le facteur U est vraiment à considérer et à évaluer. "
            },
            {
                tag: "sousTitre",
                txt: "5e étape : restez à l’abri des intempéries grâce aux fenêtres performantes"
            },
            {
                tag: "texte",
                txt: "Le choix d’une fenêtre s’arrête également sur son niveau de performance : est-elle non seulement solide et efficace, mais résiste-t-elle aussi au vent, à l’eau et à l’air? On se le rappelle : les fenêtres doivent être un gage de sécurité et de confort! Pour connaître leur niveau de performance, vous n’avez qu’à demander leurs résultats auprès de votre détaillant. Pour obtenir de bons conseils sur l'achat de fenêtres, n’hésitez pas à contacter l’équipe de Fenêtres Jalmo qui se fera un réel plaisir de vous aider."
            },
            {
                tag: "sousTitre",
                txt: "6e étape : prenez des mesures pour une estimation précise"
            },
            {
                tag: "texte",
                txt: "Hauteur, largeur et profondeur de vos fenêtres : si ce n’est déjà fait, sortez calepin ou cellulaire pour les y noter! N’oubliez pas dans vos calculs les moulures pour ce qui est de la hauteur et de la largeur! Ainsi, en ayant en mains toutes les mesures, vous pourrez obtenir une estimation plus juste en évitant de gros écarts de prix… et de mauvaises surprises. "
            },
            {
                tag: "sousTitre",
                txt: "7e étape : demande d’estimation de votre projet de fenêtres"
            },
            {
                tag: "texte",
                txt: "Étape importante qui influencera votre décision : l’estimation du prix de vos fenêtres (accessoires et quincaillerie) incluant la garantie et les frais d’installation! Jetez-y bien un coup d'œil pour que tous ces éléments-là s’y trouvent. Ne vous gênez pas aussi pour poser des questions sur les détails de votre estimation. L’achat de fenêtres est un gros investissement, il vaut mieux donc bien s’y préparer et savoir où iront les coûts. Pour une estimation de qualité et transparente, faites affaire avec l’équipe de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "8e étape : optez pour un manufacturier québécois"
            },
            {
                tag: "texte",
                txt: "En osant la fabrication locale de vos fenêtres, vous vous garantissez un produit de qualité, performant, durable, fiable et conçu pour toutes les saisons québécoises. Il ne faut donc pas lésiner sur la qualité supérieure d’une conception locale surtout quand il est question de votre confort et de votre sécurité. Une promesse que s’engage à tenir Fenêtres Jalmo, manufacturier québécois de fenêtres au service de votre bien-être."
            },
            {
                tag: "texte",
                txt: "Vous désirez remplacer vos portes et fenêtres pour des produits à haut rendement énergétique, de qualité, durables, performants et conçus au Québec ? Pour toutes questions ou une estimation, contactez l’équipe de Fenêtres Jalmo qui se fera un plaisir de vous servir !"
            }
        ]
    },

    {
        "path": "blogue",
        "slug": "guide-complet-pour-lachat-de-nouvelles-fenetres",

        "images": "fj-blogue-placeholder.png",
        "cat": "Conseils",
        "date": "12 Juillet 2021",
        "title": "Le guide complet pour l’achat de nouvelles fenêtres : 8 étapes importantes à suivre",
        "preview": "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides!",
        "intro": [
            "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides! Vous êtes à un clic de magasiner vos fenêtres sur la grande Toile mondiale, mais ne savez quelles étapes suivre pour circonscrire exactement votre choix? Du type d’ouvrant en passant par le bon cadre à sélectionner jusqu’à la quincaillerie de vos fenêtres, nous passons en revue tous les éléments!",
            "C’est ici qu’on vous vient en aide pour faire en sorte que vos futures fenêtres répondent en tous points à vos besoins, au style que vous recherchez et très certainement à votre budget. Par contre, gardez en mémoire qu’on ne peut magasiner seulement un prix lorsqu’il est question de fenêtres. L’enjeu de votre confort et de votre sécurité est trop important! Il faut donc vraiment prendre le temps de trouver un produit de qualité, performant et durable qui s'agence parfaitement à vos besoins esthétiques tout en respectant les sous que vous êtes prêts à verser.",
            "Voici donc sans plus tarder 8 étapes importantes à suivre pour l’achat de meilleures fenêtres! "
        ],
        "dsc": [
            {
                tag: "sousTitre",
                txt: "1re étape : pensez «municipalité"
            },
            {
                tag: "texte",
                txt: "Vous savez qu’un permis est requis pour certaines rénovations majeures à faire dans votre maison, mais est-ce le cas pour le remplacement de vos fenêtres? Pour le savoir, informez-vous auprès de votre municipalité. Celle-ci vous octroiera un permis si cela est prescrit. Aussi, vous serez surpris d'apprendre que, pour une question d’esthétique et d’uniformité, votre municipalité peut vous obliger à remplacer vos fenêtres par un style d’ouverture précis (fenêtres à battant ou à auvent, par exemple). Cette information influencera par ailleurs le choix que vous ferez ultérieurement. Il est donc important d’être au courant avant de faire l’achat de nouvelles fenêtres."
            },
            {
                tag: "sousTitre",
                txt: "2e étape :  quel type d’ouvrant choisir pour vos fenêtres?"
            },
            {
                tag: "texte",
                txt: "Fenêtres à battant, à auvent, coulissantes ou à guillotine? Il se peut que vous sachiez déjà ce qu’il vous faut comme type d’ouvrant ou que vous deviez suivre les règles municipales vous prescrivant le style d’ouverture; or, si ce n’est pas le cas, il faut tenir compte de ce chacune des ouvertures de fenêtres peut vous apporter en performance, en confort et en design. Prêt à les découvrir?"
            },

            {
                tag: "sousTitre",
                txt: "À battant"
            },
            {
                tag: "texte",
                txt: "Par exemple, si vous désirez une ouverture facile d’entretien, verticale, à manivelle, qui s’effectue vers l’extérieur et qui offre une performance supérieure ainsi qu’une excellente aération, votre choix s’arrêtera sur des fenêtres à battant, option particulièrement prisée au Québec."
            },
            {
                tag: "sousTitre",
                txt: "À auvent"
            },
            {
                tag: "texte",
                txt: "Or, si le look que vous désirez ressemble davantage à une ouverture horizontale, il se peut que les fenêtres à auvent offrant également une bonne ventilation ainsi qu’une excellente performance et permettant une ouverture vers l’extérieur soient votre choix de prédilection. Autre petit point à savoir : ces fenêtres peuvent demeurer ouvertes lorsqu’il pleut."
            },
            {
                tag: "sousTitre",
                txt: "Coulissantes"
            },
            {
                tag: "texte",
                txt: "Munies d’un système de loquet et de ressorts intégrés facilitant l’ouverture par un glissement horizontal, les fenêtres coulissantes, offertes en 2 ouvrants ou en un seul, prennent peu d’espace. Ce type de fenêtres qui s’agence bien aux styles contemporain et colonial se veut une option économique. "
            },
            {
                tag: "sousTitre",
                txt: "À guillotine"
            },
            {
                tag: "texte",
                txt: "Si vous êtes plus du style traditionnel, les fenêtres à guillotine, offrant une ouverture à bascule du volet vers l’intérieur, un système performant contre les infiltrations d’air et un système intégré de rejet d’eau, peuvent s’avérer un excellent choix."
            },
            {
                tag: "texte",
                txt: "Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure.Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure."
            },
            {
                tag: "sousTitre",
                txt: "3e étape : fenêtres en PVC ou hybrides?"
            },
            {
                tag: "texte",
                txt: "Une fois le type d’ouverture choisi vient le temps de sélectionner le cadre des fenêtres correspondant à vos besoins, à votre style et à votre budget. Voici certains critères à considérer pour chacun des types de cadres. "
            },
            {
                tag: "texte",
                txt: "Pour davantage de conseils et d’informations vous aidant à faire votre choix, nous vous invitons à contacter l’équipe-conseil de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "4e étape : efficacité énergétique"
            },
            {
                tag: "texte",
                txt: "Prévenez les pertes d’énergie et restez au chaud en optant pour des fenêtres certifiées ENERGY STAR. Celles-ci réduiront assurément vos factures d’électricité et vous assureront un doux confort. Par contre, il faut aussi considérer le facteur U (indiquant le degré de transmission de chaleur ou de froid par la fenêtre) lorsque vous achetez de nouvelles fenêtres. En fait, plus son facteur est faible, plus la fenêtre est performante et efficace. Ainsi, si certaines de vos fenêtres sont situées plus au soleil ou face au vent, le facteur U est vraiment à considérer et à évaluer. "
            },
            {
                tag: "sousTitre",
                txt: "5e étape : restez à l’abri des intempéries grâce aux fenêtres performantes"
            },
            {
                tag: "texte",
                txt: "Le choix d’une fenêtre s’arrête également sur son niveau de performance : est-elle non seulement solide et efficace, mais résiste-t-elle aussi au vent, à l’eau et à l’air? On se le rappelle : les fenêtres doivent être un gage de sécurité et de confort! Pour connaître leur niveau de performance, vous n’avez qu’à demander leurs résultats auprès de votre détaillant. Pour obtenir de bons conseils sur l'achat de fenêtres, n’hésitez pas à contacter l’équipe de Fenêtres Jalmo qui se fera un réel plaisir de vous aider."
            },
            {
                tag: "sousTitre",
                txt: "6e étape : prenez des mesures pour une estimation précise"
            },
            {
                tag: "texte",
                txt: "Hauteur, largeur et profondeur de vos fenêtres : si ce n’est déjà fait, sortez calepin ou cellulaire pour les y noter! N’oubliez pas dans vos calculs les moulures pour ce qui est de la hauteur et de la largeur! Ainsi, en ayant en mains toutes les mesures, vous pourrez obtenir une estimation plus juste en évitant de gros écarts de prix… et de mauvaises surprises. "
            },
            {
                tag: "sousTitre",
                txt: "7e étape : demande d’estimation de votre projet de fenêtres"
            },
            {
                tag: "texte",
                txt: "Étape importante qui influencera votre décision : l’estimation du prix de vos fenêtres (accessoires et quincaillerie) incluant la garantie et les frais d’installation! Jetez-y bien un coup d'œil pour que tous ces éléments-là s’y trouvent. Ne vous gênez pas aussi pour poser des questions sur les détails de votre estimation. L’achat de fenêtres est un gros investissement, il vaut mieux donc bien s’y préparer et savoir où iront les coûts. Pour une estimation de qualité et transparente, faites affaire avec l’équipe de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "8e étape : optez pour un manufacturier québécois"
            },
            {
                tag: "texte",
                txt: "En osant la fabrication locale de vos fenêtres, vous vous garantissez un produit de qualité, performant, durable, fiable et conçu pour toutes les saisons québécoises. Il ne faut donc pas lésiner sur la qualité supérieure d’une conception locale surtout quand il est question de votre confort et de votre sécurité. Une promesse que s’engage à tenir Fenêtres Jalmo, manufacturier québécois de fenêtres au service de votre bien-être."
            },
            {
                tag: "texte",
                txt: "Vous désirez remplacer vos portes et fenêtres pour des produits à haut rendement énergétique, de qualité, durables, performants et conçus au Québec ? Pour toutes questions ou une estimation, contactez l’équipe de Fenêtres Jalmo qui se fera un plaisir de vous servir !"
            }
        ]
    },

    {
        "path": "blogue",
        "slug": "guide-complet-pour-lachat-de-nouvelles-fenetres",

        "images": "fj-blogue-placeholder.png",
        "cat": "Conseils",
        "date": "12 Juillet 2021",
        "title": "Le guide complet pour l’achat de nouvelles fenêtres : 8 étapes importantes à suivre",
        "preview": "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides!",
        "intro": [
            "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides! Vous êtes à un clic de magasiner vos fenêtres sur la grande Toile mondiale, mais ne savez quelles étapes suivre pour circonscrire exactement votre choix? Du type d’ouvrant en passant par le bon cadre à sélectionner jusqu’à la quincaillerie de vos fenêtres, nous passons en revue tous les éléments!",
            "C’est ici qu’on vous vient en aide pour faire en sorte que vos futures fenêtres répondent en tous points à vos besoins, au style que vous recherchez et très certainement à votre budget. Par contre, gardez en mémoire qu’on ne peut magasiner seulement un prix lorsqu’il est question de fenêtres. L’enjeu de votre confort et de votre sécurité est trop important! Il faut donc vraiment prendre le temps de trouver un produit de qualité, performant et durable qui s'agence parfaitement à vos besoins esthétiques tout en respectant les sous que vous êtes prêts à verser.",
            "Voici donc sans plus tarder 8 étapes importantes à suivre pour l’achat de meilleures fenêtres! "
        ],
        "dsc": [
            {
                tag: "sousTitre",
                txt: "1re étape : pensez «municipalité"
            },
            {
                tag: "texte",
                txt: "Vous savez qu’un permis est requis pour certaines rénovations majeures à faire dans votre maison, mais est-ce le cas pour le remplacement de vos fenêtres? Pour le savoir, informez-vous auprès de votre municipalité. Celle-ci vous octroiera un permis si cela est prescrit. Aussi, vous serez surpris d'apprendre que, pour une question d’esthétique et d’uniformité, votre municipalité peut vous obliger à remplacer vos fenêtres par un style d’ouverture précis (fenêtres à battant ou à auvent, par exemple). Cette information influencera par ailleurs le choix que vous ferez ultérieurement. Il est donc important d’être au courant avant de faire l’achat de nouvelles fenêtres."
            },
            {
                tag: "sousTitre",
                txt: "2e étape :  quel type d’ouvrant choisir pour vos fenêtres?"
            },
            {
                tag: "texte",
                txt: "Fenêtres à battant, à auvent, coulissantes ou à guillotine? Il se peut que vous sachiez déjà ce qu’il vous faut comme type d’ouvrant ou que vous deviez suivre les règles municipales vous prescrivant le style d’ouverture; or, si ce n’est pas le cas, il faut tenir compte de ce chacune des ouvertures de fenêtres peut vous apporter en performance, en confort et en design. Prêt à les découvrir?"
            },

            {
                tag: "sousTitre",
                txt: "À battant"
            },
            {
                tag: "texte",
                txt: "Par exemple, si vous désirez une ouverture facile d’entretien, verticale, à manivelle, qui s’effectue vers l’extérieur et qui offre une performance supérieure ainsi qu’une excellente aération, votre choix s’arrêtera sur des fenêtres à battant, option particulièrement prisée au Québec."
            },
            {
                tag: "sousTitre",
                txt: "À auvent"
            },
            {
                tag: "texte",
                txt: "Or, si le look que vous désirez ressemble davantage à une ouverture horizontale, il se peut que les fenêtres à auvent offrant également une bonne ventilation ainsi qu’une excellente performance et permettant une ouverture vers l’extérieur soient votre choix de prédilection. Autre petit point à savoir : ces fenêtres peuvent demeurer ouvertes lorsqu’il pleut."
            },
            {
                tag: "sousTitre",
                txt: "Coulissantes"
            },
            {
                tag: "texte",
                txt: "Munies d’un système de loquet et de ressorts intégrés facilitant l’ouverture par un glissement horizontal, les fenêtres coulissantes, offertes en 2 ouvrants ou en un seul, prennent peu d’espace. Ce type de fenêtres qui s’agence bien aux styles contemporain et colonial se veut une option économique. "
            },
            {
                tag: "sousTitre",
                txt: "À guillotine"
            },
            {
                tag: "texte",
                txt: "Si vous êtes plus du style traditionnel, les fenêtres à guillotine, offrant une ouverture à bascule du volet vers l’intérieur, un système performant contre les infiltrations d’air et un système intégré de rejet d’eau, peuvent s’avérer un excellent choix."
            },
            {
                tag: "texte",
                txt: "Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure.Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure."
            },
            {
                tag: "sousTitre",
                txt: "3e étape : fenêtres en PVC ou hybrides?"
            },
            {
                tag: "texte",
                txt: "Une fois le type d’ouverture choisi vient le temps de sélectionner le cadre des fenêtres correspondant à vos besoins, à votre style et à votre budget. Voici certains critères à considérer pour chacun des types de cadres. "
            },
            {
                tag: "texte",
                txt: "Pour davantage de conseils et d’informations vous aidant à faire votre choix, nous vous invitons à contacter l’équipe-conseil de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "4e étape : efficacité énergétique"
            },
            {
                tag: "texte",
                txt: "Prévenez les pertes d’énergie et restez au chaud en optant pour des fenêtres certifiées ENERGY STAR. Celles-ci réduiront assurément vos factures d’électricité et vous assureront un doux confort. Par contre, il faut aussi considérer le facteur U (indiquant le degré de transmission de chaleur ou de froid par la fenêtre) lorsque vous achetez de nouvelles fenêtres. En fait, plus son facteur est faible, plus la fenêtre est performante et efficace. Ainsi, si certaines de vos fenêtres sont situées plus au soleil ou face au vent, le facteur U est vraiment à considérer et à évaluer. "
            },
            {
                tag: "sousTitre",
                txt: "5e étape : restez à l’abri des intempéries grâce aux fenêtres performantes"
            },
            {
                tag: "texte",
                txt: "Le choix d’une fenêtre s’arrête également sur son niveau de performance : est-elle non seulement solide et efficace, mais résiste-t-elle aussi au vent, à l’eau et à l’air? On se le rappelle : les fenêtres doivent être un gage de sécurité et de confort! Pour connaître leur niveau de performance, vous n’avez qu’à demander leurs résultats auprès de votre détaillant. Pour obtenir de bons conseils sur l'achat de fenêtres, n’hésitez pas à contacter l’équipe de Fenêtres Jalmo qui se fera un réel plaisir de vous aider."
            },
            {
                tag: "sousTitre",
                txt: "6e étape : prenez des mesures pour une estimation précise"
            },
            {
                tag: "texte",
                txt: "Hauteur, largeur et profondeur de vos fenêtres : si ce n’est déjà fait, sortez calepin ou cellulaire pour les y noter! N’oubliez pas dans vos calculs les moulures pour ce qui est de la hauteur et de la largeur! Ainsi, en ayant en mains toutes les mesures, vous pourrez obtenir une estimation plus juste en évitant de gros écarts de prix… et de mauvaises surprises. "
            },
            {
                tag: "sousTitre",
                txt: "7e étape : demande d’estimation de votre projet de fenêtres"
            },
            {
                tag: "texte",
                txt: "Étape importante qui influencera votre décision : l’estimation du prix de vos fenêtres (accessoires et quincaillerie) incluant la garantie et les frais d’installation! Jetez-y bien un coup d'œil pour que tous ces éléments-là s’y trouvent. Ne vous gênez pas aussi pour poser des questions sur les détails de votre estimation. L’achat de fenêtres est un gros investissement, il vaut mieux donc bien s’y préparer et savoir où iront les coûts. Pour une estimation de qualité et transparente, faites affaire avec l’équipe de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "8e étape : optez pour un manufacturier québécois"
            },
            {
                tag: "texte",
                txt: "En osant la fabrication locale de vos fenêtres, vous vous garantissez un produit de qualité, performant, durable, fiable et conçu pour toutes les saisons québécoises. Il ne faut donc pas lésiner sur la qualité supérieure d’une conception locale surtout quand il est question de votre confort et de votre sécurité. Une promesse que s’engage à tenir Fenêtres Jalmo, manufacturier québécois de fenêtres au service de votre bien-être."
            },
            {
                tag: "texte",
                txt: "Vous désirez remplacer vos portes et fenêtres pour des produits à haut rendement énergétique, de qualité, durables, performants et conçus au Québec ? Pour toutes questions ou une estimation, contactez l’équipe de Fenêtres Jalmo qui se fera un plaisir de vous servir !"
            }
        ]
    },

    {
        "path": "blogue",
        "slug": "guide-complet-pour-lachat-de-nouvelles-fenetres",

        "images": "fj-blogue-placeholder.png",
        "cat": "Conseils",
        "date": "12 Juillet 2021",
        "title": "Le guide complet pour l’achat de nouvelles fenêtres : 8 étapes importantes à suivre",
        "preview": "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides!",
        "intro": [
            "Vous ne pouvez plus éviter l’inévitable : les fenêtres de votre maison doivent être remplacées, si ce n’est qu'elles ne vous offrent plus confort et chaleur pendant les saisons froides! Vous êtes à un clic de magasiner vos fenêtres sur la grande Toile mondiale, mais ne savez quelles étapes suivre pour circonscrire exactement votre choix? Du type d’ouvrant en passant par le bon cadre à sélectionner jusqu’à la quincaillerie de vos fenêtres, nous passons en revue tous les éléments!",
            "C’est ici qu’on vous vient en aide pour faire en sorte que vos futures fenêtres répondent en tous points à vos besoins, au style que vous recherchez et très certainement à votre budget. Par contre, gardez en mémoire qu’on ne peut magasiner seulement un prix lorsqu’il est question de fenêtres. L’enjeu de votre confort et de votre sécurité est trop important! Il faut donc vraiment prendre le temps de trouver un produit de qualité, performant et durable qui s'agence parfaitement à vos besoins esthétiques tout en respectant les sous que vous êtes prêts à verser.",
            "Voici donc sans plus tarder 8 étapes importantes à suivre pour l’achat de meilleures fenêtres! "
        ],
        "dsc": [
            {
                tag: "sousTitre",
                txt: "1re étape : pensez «municipalité"
            },
            {
                tag: "texte",
                txt: "Vous savez qu’un permis est requis pour certaines rénovations majeures à faire dans votre maison, mais est-ce le cas pour le remplacement de vos fenêtres? Pour le savoir, informez-vous auprès de votre municipalité. Celle-ci vous octroiera un permis si cela est prescrit. Aussi, vous serez surpris d'apprendre que, pour une question d’esthétique et d’uniformité, votre municipalité peut vous obliger à remplacer vos fenêtres par un style d’ouverture précis (fenêtres à battant ou à auvent, par exemple). Cette information influencera par ailleurs le choix que vous ferez ultérieurement. Il est donc important d’être au courant avant de faire l’achat de nouvelles fenêtres."
            },
            {
                tag: "sousTitre",
                txt: "2e étape :  quel type d’ouvrant choisir pour vos fenêtres?"
            },
            {
                tag: "texte",
                txt: "Fenêtres à battant, à auvent, coulissantes ou à guillotine? Il se peut que vous sachiez déjà ce qu’il vous faut comme type d’ouvrant ou que vous deviez suivre les règles municipales vous prescrivant le style d’ouverture; or, si ce n’est pas le cas, il faut tenir compte de ce chacune des ouvertures de fenêtres peut vous apporter en performance, en confort et en design. Prêt à les découvrir?"
            },

            {
                tag: "sousTitre",
                txt: "À battant"
            },
            {
                tag: "texte",
                txt: "Par exemple, si vous désirez une ouverture facile d’entretien, verticale, à manivelle, qui s’effectue vers l’extérieur et qui offre une performance supérieure ainsi qu’une excellente aération, votre choix s’arrêtera sur des fenêtres à battant, option particulièrement prisée au Québec."
            },
            {
                tag: "sousTitre",
                txt: "À auvent"
            },
            {
                tag: "texte",
                txt: "Or, si le look que vous désirez ressemble davantage à une ouverture horizontale, il se peut que les fenêtres à auvent offrant également une bonne ventilation ainsi qu’une excellente performance et permettant une ouverture vers l’extérieur soient votre choix de prédilection. Autre petit point à savoir : ces fenêtres peuvent demeurer ouvertes lorsqu’il pleut."
            },
            {
                tag: "sousTitre",
                txt: "Coulissantes"
            },
            {
                tag: "texte",
                txt: "Munies d’un système de loquet et de ressorts intégrés facilitant l’ouverture par un glissement horizontal, les fenêtres coulissantes, offertes en 2 ouvrants ou en un seul, prennent peu d’espace. Ce type de fenêtres qui s’agence bien aux styles contemporain et colonial se veut une option économique. "
            },
            {
                tag: "sousTitre",
                txt: "À guillotine"
            },
            {
                tag: "texte",
                txt: "Si vous êtes plus du style traditionnel, les fenêtres à guillotine, offrant une ouverture à bascule du volet vers l’intérieur, un système performant contre les infiltrations d’air et un système intégré de rejet d’eau, peuvent s’avérer un excellent choix."
            },
            {
                tag: "texte",
                txt: "Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure.Pour vous guider précisément, demandez conseil à l’équipe de Fenêtres Jalmo, fabricant québécois de fenêtres durables, performantes et de qualité supérieure."
            },
            {
                tag: "sousTitre",
                txt: "3e étape : fenêtres en PVC ou hybrides?"
            },
            {
                tag: "texte",
                txt: "Une fois le type d’ouverture choisi vient le temps de sélectionner le cadre des fenêtres correspondant à vos besoins, à votre style et à votre budget. Voici certains critères à considérer pour chacun des types de cadres. "
            },
            {
                tag: "texte",
                txt: "Pour davantage de conseils et d’informations vous aidant à faire votre choix, nous vous invitons à contacter l’équipe-conseil de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "4e étape : efficacité énergétique"
            },
            {
                tag: "texte",
                txt: "Prévenez les pertes d’énergie et restez au chaud en optant pour des fenêtres certifiées ENERGY STAR. Celles-ci réduiront assurément vos factures d’électricité et vous assureront un doux confort. Par contre, il faut aussi considérer le facteur U (indiquant le degré de transmission de chaleur ou de froid par la fenêtre) lorsque vous achetez de nouvelles fenêtres. En fait, plus son facteur est faible, plus la fenêtre est performante et efficace. Ainsi, si certaines de vos fenêtres sont situées plus au soleil ou face au vent, le facteur U est vraiment à considérer et à évaluer. "
            },
            {
                tag: "sousTitre",
                txt: "5e étape : restez à l’abri des intempéries grâce aux fenêtres performantes"
            },
            {
                tag: "texte",
                txt: "Le choix d’une fenêtre s’arrête également sur son niveau de performance : est-elle non seulement solide et efficace, mais résiste-t-elle aussi au vent, à l’eau et à l’air? On se le rappelle : les fenêtres doivent être un gage de sécurité et de confort! Pour connaître leur niveau de performance, vous n’avez qu’à demander leurs résultats auprès de votre détaillant. Pour obtenir de bons conseils sur l'achat de fenêtres, n’hésitez pas à contacter l’équipe de Fenêtres Jalmo qui se fera un réel plaisir de vous aider."
            },
            {
                tag: "sousTitre",
                txt: "6e étape : prenez des mesures pour une estimation précise"
            },
            {
                tag: "texte",
                txt: "Hauteur, largeur et profondeur de vos fenêtres : si ce n’est déjà fait, sortez calepin ou cellulaire pour les y noter! N’oubliez pas dans vos calculs les moulures pour ce qui est de la hauteur et de la largeur! Ainsi, en ayant en mains toutes les mesures, vous pourrez obtenir une estimation plus juste en évitant de gros écarts de prix… et de mauvaises surprises. "
            },
            {
                tag: "sousTitre",
                txt: "7e étape : demande d’estimation de votre projet de fenêtres"
            },
            {
                tag: "texte",
                txt: "Étape importante qui influencera votre décision : l’estimation du prix de vos fenêtres (accessoires et quincaillerie) incluant la garantie et les frais d’installation! Jetez-y bien un coup d'œil pour que tous ces éléments-là s’y trouvent. Ne vous gênez pas aussi pour poser des questions sur les détails de votre estimation. L’achat de fenêtres est un gros investissement, il vaut mieux donc bien s’y préparer et savoir où iront les coûts. Pour une estimation de qualité et transparente, faites affaire avec l’équipe de Fenêtres Jalmo."
            },
            {
                tag: "sousTitre",
                txt: "8e étape : optez pour un manufacturier québécois"
            },
            {
                tag: "texte",
                txt: "En osant la fabrication locale de vos fenêtres, vous vous garantissez un produit de qualité, performant, durable, fiable et conçu pour toutes les saisons québécoises. Il ne faut donc pas lésiner sur la qualité supérieure d’une conception locale surtout quand il est question de votre confort et de votre sécurité. Une promesse que s’engage à tenir Fenêtres Jalmo, manufacturier québécois de fenêtres au service de votre bien-être."
            },
            {
                tag: "texte",
                txt: "Vous désirez remplacer vos portes et fenêtres pour des produits à haut rendement énergétique, de qualité, durables, performants et conçus au Québec ? Pour toutes questions ou une estimation, contactez l’équipe de Fenêtres Jalmo qui se fera un plaisir de vous servir !"
            }
        ]
    }
];

export default articles;